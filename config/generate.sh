#! /bin/bash
sed -i "s/PLACEHOLDER_DATABASE_HOST/$MATOMO_DATABASE_HOST/" /var/www/html/config/config.ini.php
sed -i "s/PLACEHOLDER_DATABASE_USERNAME/$MATOMO_DATABASE_USERNAME/" /var/www/html/config/config.ini.php
sed -i "s/PLACEHOLDER_DATABASE_PASSWORD/$MATOMO_DATABASE_PASSWORD/" /var/www/html/config/config.ini.php
sed -i "s/PLACEHOLDER_DATABASE_DBNAME/$MATOMO_DATABASE_DBNAME/" /var/www/html/config/config.ini.php
sed -i "s/PLACEHOLDER_DATABASE_TABLE_PREFIX/$MATOMO_DATABASE_TABLE_PREFIX/" /var/www/html/config/config.ini.php
sed -i "s/PLACEHOLDER_SALT/$MATOMO_SALT/" /var/www/html/config/config.ini.php
sed -i "s/PLACEHOLDER_TRUSTED_HOST/$MATOMO_TRUSTED_HOST/" /var/www/html/config/config.ini.php
sed -i "s/PLACEHOLDER_DATABASE_PORT/$MATOMO_DATABASE_PORT/" /var/www/html/config/config.ini.php
