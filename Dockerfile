FROM matomo:3.13.6

COPY config/config.ini.php /var/www/html/config/config.ini.php
COPY config/generate.sh /usr/src/generate.sh

COPY docker-entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["apache2-foreground"]
