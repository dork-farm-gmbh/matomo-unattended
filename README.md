build on top of official matomo image to allow app to run after container restart

basic wrapper around official `matomo` image which allows the container to start with settings grabbed from ENV, without human intervention.

the official image does not include `/var/www/html/config/config.ini.php` file, without which the app does not actually start; in the official image, this file is generated upon completion of initial install guide (which already auto-populates form fields with values read from ENV) and is lost upon container restart

this image takes care of that by generating `config.ini.php` upon container start based on a template file and ENV values
as of now, **there are no safety checks!**

if the DB is not already set up, you need to delete config.php.ini. this will get your new matomo to go into initial setup. after finishing the setup you can and should restart the container.
